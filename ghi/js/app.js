function createCard(name, description, pictureUrl, starts, ends,location) {
    return `
        <div class="card" style="width: 18rem; margin:20px 10px; box-shadow: inset 0px -3px 15px 3px; ">
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text"><small class="text-muted">${location}</small></p>
                <p class="card-text">${description}</p>
                <div class="card-footer">
                    <small class="text-muted">${(new Date(starts)).toLocaleDateString()} - ${(new Date(ends)).toLocaleDateString()}</small>
                </div>
            </div>
        </div>
    `;

}

function placeholderCard() {
    return `
    <div class="card" aria-hidden="true"  style="width: 18rem; height: 30rem;">
        <div class="card-body">
            <h5 class="card-title placeholder-glow">
                <span class="placeholder col-6"></span>
            </h5>
            <p class="card-text placeholder-glow">
                <span class="placeholder col-7"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-6"></span>
                <span class="placeholder col-8"></span>
            </p>
        </div>
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        if (!response.ok){
            throw new Error('Response not ok');
        }else{
            const data = await response.json();
            // const conference=data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

            // const detailUrl=`http://localhost:8000${conference.href}`;
            // const detailResponse= await fetch (detailUrl);
            // if (detailResponse.ok){
            //     const details=await detailResponse.json();

            //     const conference=details.conference;
            //     const descriptionTag = document.querySelector('.card-text');
            //     descriptionTag.innerHTML = conference.description;

            //     const imageTag = document.querySelector('.card-img-top');
            //     imageTag.src = details.conference.location.picture_url;
            //     console.log(details);
            let html = "";
            const column = document.querySelector('.col');

            for (let conference of data.conferences){
                const detailUrl=`http://localhost:8000${conference.href}`;
                const placeholderHtml = placeholderCard();
                column.innerHTML += placeholderHtml;
                const detailResponse= await fetch(detailUrl);

                if(detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title= details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const location= details.conference.location.name;
                    html += createCard(title, description , pictureUrl, starts, ends,location);
                }
            }
            column.innerHTML = html;

        }
    }catch (error){
        const html =`<div class="alert alert-primary" role="alert">
                        Error occured while fetching conference info
                    </div>`;
        const column = document.querySelector('.col');
        column.innerHTML += html;
        console.error('error',error);
    }
});
